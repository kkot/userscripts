<?php
header("Access-Control-Allow-Origin: *");

if ($_SERVER['REQUEST_METHOD'] === 'POST')
  file_put_contents('data.txt', file_get_contents('php://input'));
else
  echo file_get_contents('data.txt');
