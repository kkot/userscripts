// ==UserScript==
//
// @name    hide
// @namespace kk
// @description my scripts
//
// @include https://www.wykop.pl/*
// @include *rmf24.pl*
// @include *reddit.com*
//
// @include *infoq.com/
//
// @include *trends.google.com*
// @include *github.com/*pulse*
// @include *sotagtrends.com*
// @include *insights.stackoverflow.com/trends*
// @include *data.firefox.com*
// @include *gs.statcounter.com*
//
// @include *imgur.com*
// @include *www.cda.pl*
// @include *chomikuj.pl*
// @include *wikipedia.org*
// @include https://www.youtube.comx/*
//
// @include *1337x*
// @include *rarbg*
//
//
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @require https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js
// @require https://cdnjs.cloudflare.com/ajax/libs/luxon/1.26.0/luxon.min.js
//
// @connect 0tgai2rze0.execute-api.eu-central-1.amazonaws.com
// @updateURL https://bitbucket.org/kkot/userscripts/raw/master/hide/hide.user.js
// @downloadURL https://bitbucket.org/kkot/userscripts/raw/master/hide/hide.user.js
//
// @version 2022.5.22.1
//
// @run-at document-start
// @grant GM.getValue
// @grant GM.setValue
// @grant GM_getValue
// @grant GM_setValue
// @grant GM.xmlHttpRequest
// @grant GM_addStyle
// @grant unsafeWindow
// @noframes
//
// ==/UserScript==

/* eslint-env jquery,es6,moment,luxon */

function main() {
  "use strict";

  console.log('--------------------------');
  console.log('-- START ' + getUrl());
  console.log('--------------------------');

  function inFrame() {
    return window.top !== window.self;
  }

  if (inFrame()) {
    return;
  }

  const SEC = 1000;
  const MIN = 60*SEC;
  const HOUR = 60*MIN;
  const DAY = 24*HOUR;
  const MONTH = 30*DAY;
  const YEAR = 365*DAY;

  jQuery.fn.extend({
    offOn: function (event, handler) {
      $(this).off(event).on(event, handler);
    },
    isChildOf: function (selector) {
      return $(this).closest(selector).length > 0;
    }
  });

  /**
   * RULES
   */

  let rules = [];

  rules.push({
    url: ["reddit.com"],

    async postMinVotes(minVotes) {
        return fetch("https://www.reddit.com/post/options", {
          "credentials": "include",
          "headers": {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language": "pl,en-US;q=0.7,en;q=0.3",
            "Content-Type": "application/x-www-form-urlencoded",
            "Upgrade-Insecure-Requests": "1"
          },
          "referrer": "https://www.reddit.com/",
          "body": `uh=dqsghfwru88a75c56e5619347c4e6bd7a73f30c0fe6634baee&lang=en-us&newwindow=on&media=off&media_preview=subreddit&numsites=100&min_link_score=${minVotes}&default_comment_sort=confidence&min_comment_score=-4&num_comments=200&threaded_messages=on&mark_messages_read=on&monitor_mentions=on&show_stylesheets=on&show_flair=on&show_link_flair=on&hide_from_robots=on`,
          "method": "POST",
          "mode": "cors"
        });
    },

    async after() {
      const initMinVotes = await GM.getValue("minVotes", 100);
      let buttons = "";
      let buttonsVotes = [10, 25, 50, 100];
      buttonsVotes.map(limit => {
        buttons += `<input class='minVotesNumberButton' type='button' value='${limit}'></input>`;
      });
      let input = `<p>
                     <input id='minVotesInput' type='text' value='${initMinVotes}'></input>
                     <input id='minVotesButton' type='button' value='sent'></input>
                   </p>
                   <p>
                     ${buttons}
                   </p>`;

      $(".titlebox .redditname").after(input);
      $('#minVotesButton').click(async () => {
        let minVotes = $("#minVotesInput")[0].value;
        console.log("minVotes " + minVotes);
        GM.setValue("minVotes", minVotes);
        let result = await this.postMinVotes(minVotes);
        console.log(result);
        location.reload()
      });
      $('.minVotesNumberButton').click(e => {
        $("#minVotesInput")[0].value = e.target.value;
        $('#minVotesButton').click();
      });
    }
  });

  function isMain() {
    let path = new URL(window.location.href).pathname;
    console.log(path);
    return path === '/';
  }

  rules.push({
    url: ["1337", "rar"],
    before() {
      let days = diffDays("2022-05-21T20:00:00");
      console.log(days);
      let leftDays = 30 - days;
      if (leftDays > 0) {
        transpImp("body");
        hideImportant("body");
        alert(`${days.toFixed(1)}\n\n${leftDays.toFixed(1)}`);
      }
    }
  });

  function diffDays(start) {
      let last = luxon.DateTime.fromISO(start);
      let rest = luxon.Interval.fromDateTimes(last, luxon.DateTime.now())
      return rest.length('days');
  }

  /*
  rules.push({
    url: ["pulse", "sotagtrends", "insights", "trends", "firefox", "statcounter"],
    before() {
      let days = diffDays("2021-12-30T08:07:22Z");
      let leftDays = 30 - days;
      if (leftDays > 0 && prompt(`Już ${days.toFixed(0)}\nCzy mogę to sprawdzić za \n${leftDays.toFixed(0)}\ndni? (muszę teraz)`) != "muszę teraz") {
        transpImp("body");
        hideImportant("body");
      }
    }
  });

  rules.push({
    url: ["infoq", "/r/programming"],
    before() {
      let days = diffDays("2022-01-06T07:30:22Z");
      let leftDays = 30 - days;
      if (leftDays > 0 && prompt(`Bez nowinek już ${days.toFixed(0)} dni\nczy przeżyję jeszcze ${leftDays.toFixed(0)} dni? (muszę teraz)`) != "muszę teraz") {
        transpImp("body");
        hideImportant("body");
      }
    }
  });
  */

  rules.push({
    url: ["infoq", "/r/programming", "pulse", "sotagtrends", "insights", "firefox", "statcounter"],
    before() {
      if (true) {
          return;
      }
      let day = new Date().getDay()

      if (day == 0 || day == 6) {
        transpImp("body");
        hideImportant("body");
        alert("Programowanie tylko w tygodniu?");
      }
    }
  });

  rules.push({
    url: ['wykop.pl', 'rmf24.pl'],
    before() {
      if (true) {
        return;
      }
      const dates = JSON.parse(GM_getValue("newsLast", "{}"));
      console.log(dates);

      let host = window.location.host;
      let lastStr = dates[host];
      console.log("last visit", lastStr);

      let now = luxon.DateTime.now();
      let last = lastStr == null ? now : luxon.DateTime.fromISO(lastStr);
      let rest = luxon.Interval.fromDateTimes(last, now)
      let hours = rest.length('hours');
      console.log("rest", hours);
      if (hours > 0.5 && hours < 6.0) {
        if (prompt("Raz dziennie") != "raz dziennie") {
          return;
        }
      }
      dates[host] = new Date();
      let datesStr = JSON.stringify(dates);
      console.log(datesStr);
      GM_setValue("newsLast", datesStr);
    },
  });

  const NUM_OF_REMEMBERED_IDS = 300;
  rules.push({
    url: 'wykop.pl',
    before() {
      transpNormal("body");
    },
    after() {
      const selectorZnalezisko = "ul li.iC.link";
      const selectorNajpop = "ul.menu-list li";
      const selectorWpis = "li.iC.entry";

      /**
       * Called after getting list of ids to hide.
       */
      function hideWykopStart(listToHide) {
        hideConst();
        hideUsingIds(listToHide);
        opaqueNormal("body");
      }

      function hideImages() {
        let articles = $(".article:not(.fullview)");
        articles.find(".media-content").remove();
        articles.find(".diggbox span:contains('wykop')").remove();

        $(".link").removeClass("hot");

        let popular = $(".link-list-popular");
        popular.find(".image").remove();
        popular.find(".results").css("left", "0");
        popular.find(".w-item").css("min-height", "55px");

        // article view

        let related = $(".related");
        related.find("img").remove();
        related.find(".icon-media.play").remove();

        let article = $(".article.fullview");
        article.find(".media-content").remove();

        $(".screen .videoWrapper").remove();
      }

      function linksInNewTab() {
        $("a").attr("target", "_blank");
      }

      function hideConst() {
        $("#userPanel, #dyingLinksBox").hide();
        $("a:contains('Wykop Poleca')").closest("li").hide();

        //hideImages();
        //linksInNewTab();
        setTimeout(() => hide18plus(), 0);
        loadImg();
      }

      function hide18plus() {
        //$("img[src*='/18-plus,']").closest("li").hide();
        $(".article sup:contains('18+')").closest("li").hide()
      }

      function loadImg() {
        $("img.lazy").each((_, img) => {
          let data = $(img).attr("data-original");
          if (data) {
            img.src = data;
          }
        });
      }

      function hideTags(...tags) {
        for (let tag of tags) {
          $(`a.tag:contains('${tag}')`).closest(selectorZnalezisko).hide();
        }
      }

      function hideUsingIds(toHideArray) {
        let hiddenElements = [];
        let hiddenIdsSet = new Set();

        for (let toHide of toHideArray) {
          const znalezisko       = $(ws(`a           [href   *='/link/${toHide}/' ]`)).closest(selectorZnalezisko);
          const najpopZnalezisko = $(ws(`a.clearfix  [href   *='/link/${toHide}/' ]`)).closest(selectorNajpop);
          const wpis             = $(ws(`div         [data-id   =    '${toHide}'  ]`)).closest(selectorWpis);
          const najpopWpis       = $(ws(`div.dC.mini [data-id   =    '${toHide}'  ]`));

          const elementsToHide = znalezisko.add(najpopZnalezisko).add(wpis).add(najpopWpis);

          if (elementsToHide.length > 0) {
            elementsToHide.hide();
            hiddenElements.push(elementsToHide);
            hiddenIdsSet.add(toHide);
          }

          if (elementsToHide.length > 2) { // podwójne mogą być (w popularnych i na głównej)
            alert("debug: selected more than 2");
          }
        }
        createControls();
        addControlsActions(toHideArray, hiddenIdsSet, hiddenElements);
      }

      function createControls() {
        // remove old
        $(".gmHide, .gmMikroHide, .hidePanel").remove();

        // znaleziska
        $("div.fix-tagline").append(
          "<a class='link gray gmHide affect'>ukryj e.</a>");

        // mikro
        $("li.entry.iC div.author").append(
          "<a class='link gray gmMikroHide'>ukryj wpis</a>");

        let controls = `
<div class='hidePanel'>
<a class='link gray' href="/ustawienia/czarne-listy/">czarna </a>
<a class='link gray gmList'> lista</a>
<a class='link gray gmShow'> pokaż</a>
<a class='link gray gmReset'> resetuj</a>
</div>`;

        // place controls
        $("div.grid-right").prepend(controls); // glowna
        $("div.r-block.search").prepend(controls); // hity
      }

      function addControlsActions(toHideArray, hiddenIdsSet, hiddenElements) {
        $(".gmHide").offOn('click', function () {
          let article = $(this).closest(selectorZnalezisko);
          console.log(article);
          let url = article.find("div.row a").first().attr('href');
          if (!url) {
            url = location.href;
          }
          console.log("url", url);
          let id = getIdFromLinkUrl(url);
          hideId(toHideArray, id);
        });

        $(".gmMikroHide").offOn('click', function () {
          let id = $(this).closest(selectorWpis).find("div.dC").first().attr('data-id');
          hideId(toHideArray, id);
        });

        $(".gmList").offOn('click', function () {
          displayHidden(toHideArray, hiddenIdsSet);
        });

        $(".gmShow").offOn('click', function () {
          for (let hiddenElement of hiddenElements) {
            $(hiddenElement).show();
          }
        });

        $(".gmReset").offOn('click', function () {
          saveValue([]);
          hideUsingIds([]);
          $(".gmShow").click();
        });
      }

      function confirmHideId(toHideArray, newId) {
        getTitleAsync(newId).then(title => {
          if (confirm(`${newId} : ${title}`)) {
            hideId(toHideArray, newId);
          }
        });
      }

      /**
       *
       * @param {string[]} toHideArray
       * @param {string} newId
       */
      function hideId(toHideArray, newId) {
        const newIdNum = parseInt(newId);
        console.log("saving id to hide", newIdNum);
        toHideArray.unshift(newIdNum);
        saveValue(toHideArray.slice(0, NUM_OF_REMEMBERED_IDS));
        hideUsingIds(toHideArray);
      }

      function getTitleAsync(id) {
        return $.ajax("https://www.wykop.pl/link/" + id).then(resultLink => {
          if (resultLink.error !== undefined) {
            console.log("debug: nie znalezisko, message: " + resultLink.error);
            return $.ajax("https://www.wykop.pl/wpis/" + id);
          }
          //console.log("link, result " + resultLink);
          return resultLink;
        }).then(result => result.match("<title>((.|[\r\n])*?)<\/title>")[1]);
      }

      function displayHidden(allIdsToHide, hiddenIds) {
        let hiddenIdsArray = Array.from(hiddenIds);
        let descriptionsPromises = hiddenIdsArray.map(hiddenId => getTitleAsync(hiddenId));

        Promise.all(descriptionsPromises).then(descriptions => {
          let result = `wszystkich ${allIdsToHide.length}\n\n`;
          hiddenIdsArray.forEach(function (hiddenId, i) {
            result += `${descriptions[i]}\n`;
          });
          alert(result);
        });
      }

      const WYKOP_SAVE_URL = "https://0tgai2rze0.execute-api.eu-central-1.amazonaws.com/Stage";

      function saveValue(ids) {
        const dataToSave = JSON.stringify({ 'hide_list' : ids });
        console.log("saving", dataToSave);
        GM.xmlHttpRequest({
          method: "POST",
          url: WYKOP_SAVE_URL,
          data: dataToSave,
          onload: function (response) {
            console.log(response);
            console.log("status", response.status);
            console.log("response", response.responseText);
          },
          onerror: function (xhr, status) {
            alert("error while saving");
          }
        });
      }

      function getIdFromLinkUrl(url) {
        return /\/(\d+)\//.exec(url)[1];
      }

      (function test() {
        assertEqual("2597563", getIdFromLinkUrl("http://www.wykop.pl/link/2597563/cala-prawda-o-zus-historia-pewnej-rencistki/"));
      })();

      httpGet(WYKOP_SAVE_URL, (response) => {
        let listToHide = JSON.parse(response);
        console.log(`HTTP response - list of ids`, listToHide);
        hideWykopStart(listToHide);
      });
    }
  });

  rules.push({
    url: "wikipedia.org",
    after() {
      setTimeout(() => {
        GM_addStyle(".gmHide { display: none !important}");
        const langsToShow = ["pl", "en", "de", "es", "ru"];
        let langToItemMap = new Map();
        $("#p-lang li").each(function () {
          let li = $(this);
          let langAttr = li.find("a").attr("lang");
          if (langAttr === undefined) {
            return;
          }
          langToItemMap.set(langAttr, li);
          if (!langsToShow.includes(langAttr)) {
            li.addClass("gmHide");
          }
        });
        for (let lang of (langsToShow.reverse())) {
          if (lang in langToItemMap) {
            let li = langToItemMap.get(lang);
            $("#p-lang ul").prepend(li);
          }
        }
        $("#p-lang ul").append(`<li><a id='other_langs'>... ${langToItemMap.size}</a></li>`);
        $("#other_langs").offOn('click', function () {
          for (let el of langToItemMap.values()) {
            $(el).removeClass("gmHide");
          }
        });
        $("#p-navigation").after($("#p-lang")); // move higher
      }, 100);
    }
  });


  rules.push({
    url: "www.cda.pl",
    before() {
      transpNormal("#najaktywniejsi img, img.thumb-mini, .video-clip-link img, .minaitura img");
    }
  });

  rules.push({
    url: "imgur.com",
    before() {
      hideNormal("#side-gallery");
    }
  });

  rules.push({
    url: "chomikuj.pl",
    before() {
      hideImportant("#avatarList, #chatContainer, #ChomikFriendsPanel");
    }
  });

  let youtubeUI = {
    selVideo: "video, .ytp-preview, .ytp-storyboard-framepreview, .ytp-storyboard, .ytp-hover-progress, .ytp-thumbnail-overlay-image",
    selNonVideo: "#related img, ytd-comments img, ytd-playlist-panel-renderer img, ytd-thumbnail img, .ytd-thumbnail img" +
    "#hover-overlays, #mouseover-overlay",
    selEndScreenText: ".html5-endscreen .ytp-videowall-still-info-content",
    selEndScreenImage: ".html5-endscreen .ytp-videowall-still-image",
    hideVideo: function () {
      transpImp(this.selVideo);
    },
    showVideo: function () {
      opaqueImp(this.selVideo);
    },
    hideNonVideo: function () {
      transpNormal(this.selNonVideo);
      this.hideEndScreen();
    },
    showNonVideo: function () {
      opaqueNormal(this.selNonVideo);
      this.showEndScreen();
    },
    hideEndScreen: function () {
      opaqueNormal(this.selEndScreenText);
      transpImp(this.selEndScreenImage);
    },
    showEndScreen: function () {
      transpNormal(this.selEndScreenText);
      opaqueImp(this.selEndScreenImage);
    }
  };

  let youtubeSettings = {
    showVideo: false,
    showAll: false,
    isShowAll: function () {
      return this.showAll;
    },
    setShowAll: function (value) {
      this.showAll = value;
    },
    isShowVideo: function () {
      return this.showVideo;
    },
    setShowVideo: function (value) {
      this.showVideo = value;
    },
    isEnabled: async function() {
      return GM.getValue('enabled');
    },
    setEnabled: function (value) {
      GM.setValue('enabled', value);
    }
  };

  let youtubeUpdater = {
    ui: youtubeUI,
    settings: youtubeSettings,
    lastUrl: "",
    playerSelector: ".ytp-player-content, .html5-main-video, .html5-video-content",
    thumbSelector: ".ytd-thumbnail, .ytd-moving-thumbnail-renderer",
    endScreenSelector: ".videowall-endscreen",
    showEndScreen: false,
    initBefore: async function () {
      if (!(await this.settings.isEnabled())) {
        return;
      }
      this.ui.hideNonVideo();
      this.ui.hideVideo();
    },
    initAfter: function () {
      this.bindClickHandlers();
      youtubeToggle.schedule();
    },
    urlChanged: function () {
      let result = (this.lastUrl !== getUrl());
      this.lastUrl = getUrl();
      return result;
    },
    refresh: async function () {
      if (this.urlChanged()) {
        console.log("url changed " + getUrl());
        if (!(await this.settings.isEnabled())) {
          return;
        }
        this.hideAllClicked();
      }
    },
    showVideoClicked: function () {
      this.ui.showVideo();
      this.settings.setShowVideo(true);
    },
    hideVideoClicked: function () {
      this.ui.hideVideo();
      this.settings.setShowVideo(false);
      if (this.showEndScreen) {
        this.toggleEndScreen();
      }
    },
    showAllClicked: async function () {
      console.log("showAllClicked");
      this.showVideoClicked();
      this.ui.showNonVideo();
      this.settings.setShowAll(true);
    },
    hideAllClicked: function () {
      console.log("hideAllClicked");
      this.hideVideoClicked();
      this.ui.hideNonVideo();
      this.settings.setShowAll(false);
    },
    toggleEndScreen: function () {
      if (this.showEndScreen) {
        this.ui.hideEndScreen();
      } else {
        this.ui.showEndScreen();
      }
      this.showEndScreen = !this.showEndScreen;
    },
    bindClickHandlers: function () {
      let that = this;
      $("body").offOn('mousedown', async function (e) {
        let target = $(e.target);
        console.log(`clicked class: ${target.attr("class")}, id: ${target.attr("id")}`);
        if (rightClick(e)) {
          let playerClicked = target.isChildOf(that.playerSelector);
          let thumbClicked = target.isChildOf(that.thumbSelector);
          let endScreenClicked = target.isChildOf(that.endScreenSelector);
          if (endScreenClicked) {
            console.log("EndScreen clicked");
            that.toggleEndScreen();
          }
          else if (playerClicked) {
            console.log("Player clicked");
            if (that.settings.isShowVideo()) {
              that.hideVideoClicked();
            } else {
              that.showVideoClicked();
            }
          }
          else if (thumbClicked) {
            let showAll = that.settings.isShowAll();
            console.log("Thumb clicked, current show all " + showAll);

            if (showAll) {
              that.hideAllClicked();
            } else {
              that.showAllClicked();
            }
          }
        }
      });
      $("body").offOn('contextmenu', function (e) {
        let target = $(e.target);
        let playerClicked = target.isChildOf(that.playerSelector);
        let thumbClicked = target.isChildOf(that.thumbSelector);
        if (playerClicked) {
          that.simulateClickToDeactivateContextMenu();
        }
        if (thumbClicked) {
          e.preventDefault();
          e.stopPropagation();
        }
      });
    },
    simulateClickToDeactivateContextMenu: function () {
      if ($(".ytp-contextmenu").is(":visible")) {
        console.log("simulated click on #movie_player");
        $("#movie_player").click();
      }
    }
  };

  let youtubeToggle = {
    init: async function() {
      let container = $("ytd-topbar-logo-renderer");
      if (container.length == 0) {
        console.log("schedule")
        this.schedule();
        return;
      }
      let checkbox = $("<input id='gm_toggle' type='checkbox'></input>")[0];
      $(checkbox).on('click', this.onClick.bind(this));
      console.log(checkbox);
      console.log(container);
      container.append(checkbox);
      this.checkbox().checked = await youtubeSettings.isEnabled();
    },
    schedule: function() {
      setTimeout(() => {
        youtubeToggle.init();
      }, 1000);
    },
    checkbox: function() {
      return $('#gm_toggle')[0];
    },
    onClick: function() {
      let enabled = this.checkbox().checked;
      console.log("enabled changed to " + enabled);
      if (enabled) {
        youtubeUpdater.hideAllClicked();
      } else {
        youtubeUpdater.showAllClicked();
      }
      youtubeSettings.setEnabled(enabled);
    }
  }

  rules.push({
    url: "youtube.com",
    runAlsoInFrame: true,
    before() {
      youtubeUpdater.initBefore();
    },
    after() {
      youtubeUpdater.initAfter();
      setInterval(() => {
        youtubeUpdater.refresh();
      }, 500);
    }
  });

  //-------------------------------------------------------------------
  //               EXECUTE
  //-------------------------------------------------------------------

  function executeRules(rules, method) {
    for (let rule of rules) {
      if (!(method in rule)) {
        continue;
      }
      if (!urlHas(rule.url)) {
        continue;
      }
      if (inFrame() && rule.runAlsoInFrame !== true) {
        continue;
      }
      const start = new Date().getTime();
      rule[method]();
      //console.log("DEBUG took " + (new Date().getTime() - start) + " ms");
    }
  }

  executeRules(rules, 'before');
  $(function () {
    executeRules(rules, 'after');
  });

  //-------------------------------------------------------------------
  //               FUNCTIONS
  //-------------------------------------------------------------------

  function getUrl() {
    return location.href;
  }

  function urlHas(urlPartOrArray) {
    if (urlPartOrArray instanceof Array) {
      for (let part of urlPartOrArray) {
        if (urlHas(part)) {
          return true;
        }
      }
      return false;
    }
    return getUrl().includes(urlPartOrArray);
  }

  function addStyle(style) {
    //console.log("GM_addStyle: " + style);
    GM_addStyle(style);
  }

  function hideAll() {
    transpImp("*");
  }

  function showAll() {
    opaqueImp("*");
  }

  function hideImportant(selector) {
    addStyle(`${selector} {display:none !important}`);
  }

  function showImportant(selector) {
    addStyle(`${selector} {display:block !important}`);
  }

  function hideNormal(selector) {
    addStyle(`${selector} {display:none}`);
  }

  function showNormal(selector) {
    addStyle(`${selector} {display:block}`);
  }

  function transpImp(selector) {
    addStyle(`${selector} {opacity:0 !important}`);
  }

  function opaqueImp(selector) {
    addStyle(`${selector} {opacity:1 !important}`);
  }

  function transpNormal(selector) {
    addStyle(`${selector} {opacity:0}`);
  }

  function opaqueNormal(selector) {
    addStyle(`${selector} {opacity:1}`);
  }

  function httpGet(urlToGet, successFun) {
    console.log("HTTP GET " + urlToGet);
    GM.xmlHttpRequest({
      method: "GET",
      url: urlToGet,
      onload: function (response) {
        successFun(response.responseText);
      },
      onerror: function (response) {
        alert("GET error " + urlToGet);
      }
    });
  }

  function ws(str) {
    return str.replace(/ /g, "");
  }

  function assertEqual(exp, act) {
    if (exp != act) {
      alert("exp " + exp + " != " + act);
    }
  }

  function rightClick(e) {
    return e.which == 3;
  }

  function m(arg) {
    return moment(arg);
  }
}

main();